# Network-Data_Mining

This repository contains an attempt of using **semantic clustering** in order to classify unknown Vitis _vinifera_ genes using sequence similarity with genes from Arabidopsis _thaliana_.
