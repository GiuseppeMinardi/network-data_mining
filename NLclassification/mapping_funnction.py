"""
Stefano Marangoni 2019
"""

import pandas as pd


def mapping_function(csv_name, CheckRelation = False):
    '''This function given a dataframe, containing the gene name of Vitis
    vinifera and the corresponding the best hit homolougous sequence in
    Arabidopsis thaliana, return a dictionary that stand for the mapping
    function between the Arabidopsis genes and the Vitis genes
    '''

    df_genes=pd.read_csv(csv_name)
    f_map={}

    for i in range(df_genes.shape[0]):
        ara_gene=df_genes['Best hit At'][i].strip(" ")
        vit_gene=df_genes['Proper Vv'][i]
        temp=f_map.get(ara_gene,[])
        temp.append(vit_gene)
        temp=list(set(temp))
        f_map[ara_gene]=temp

    if CheckRelation:
        check_relation_type(f_map)

    return f_map

def check_relation_type(f_map):
    '''Check if the relationship between the At  genes and
    the Vv genes is "one to many" or "many to many"
    '''
    l=[]
    for i in f_map:
        l.extend(f_map[i])
    # mut=[]
    # for i in l:
    #     if i not in mut:
    #         if  l.count(i)>1:
    #             mut.append(i) 
    # print(mut)
    if len(l)==len(set(l)):
        print('Relation type: one to many relation')
    else:
        print('Relation type: many to many')

def label_assignment(f_map, df_ara_label):
    '''This function return a dataframe that contain the cluster label
    for the Vitis genes based on the Arabidopsis genes cluster label
    and the function that map the Vitis omologous gene for the Arabidopsis
    '''

    diz_vit_label = {}
    for i in f_map:
        label = df_ara_label.ix[:,2][df_ara_label.ix[:,0]==i].values
        for j in f_map[i]:
            temp=diz_vit_label.get(j,set())
            temp.update(label)
            diz_vit_label[j]=temp

    vit_gene_id=list(diz_vit_label.keys())

    d1={}
    d2={}
    for i in range(len(vit_gene_id)):
        d1[i]=vit_gene_id[i]
        d2[i]=" ".join([str(j) for j in diz_vit_label[vit_gene_id[i]]])

    diz_df={'MappedID':d1,'Label':d2}

    df_vit_label=pd.DataFrame(diz_df)

    return df_vit_label



if __name__ == '__main__':
    function=mapping_function('licausi_df.csv')
    import os.path

    if  os.path.isfile('fake_label.csv'):
        import random
        lab=[]
        robba=[]
        for i in range(len(function)):
            lab.append(random.randint(1,4))
            robba.append('')

        fake_lab_diz={'gene':list(function.keys()),'robba': robba ,'label':lab}

        df=pd.DataFrame(fake_lab_diz)

        df.to_csv('fake_label.csv',index=False)

    check_relation_type(function)
    fake_label=pd.read_csv('fake_label.csv')
    # print(fake_label.head())
    print(label_assignment(function,fake_label))